function boot() {  
    angular.bootstrap(document, ['posApp'], {
        strictDi: true
    });
}
document.addEventListener('DOMContentLoaded', boot); 
var app = angular.module('posApp',[]);
app.controller('posMainCtrl', ['$scope','$http', function($scope, $http){
	$scope.name="Hello dear";
	$scope.getDataFromWeb = function(){
		$http.get("https://jsonplaceholder.typicode.com/comments?postId=1").then(function(response) {
			$scope.respDataFromApi = response.data;
		 }, function(response) {
		});
	};
	$scope.getDataFromWeb();
}])