 var Greeting = React.createClass({
         getInitialState: function() {
          return { message: 'Hello, Universe Test' };
        },
        render: function() {
          return (
            <p>{this.state.message}</p>
          )
        }
      });

      ReactDOM.render(
        <Greeting/>,
        document.getElementById('greeting-div')
      );