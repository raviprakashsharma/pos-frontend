const electron = require('electron')
const {app, BrowserWindow} = electron
app.on('ready', function(){
  var mainWindow = new BrowserWindow({
  	width:1280,
  	height:850
  })
  mainWindow.loadURL('file://'+ __dirname + '/index.html')
});