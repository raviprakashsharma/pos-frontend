var Greeting = React.createClass({
  displayName: 'Greeting',

  getInitialState: function () {
    return { message: 'Hello, Universe Test' };
  },
  render: function () {
    return React.createElement(
      'p',
      null,
      this.state.message
    );
  }
});

ReactDOM.render(React.createElement(Greeting, null), document.getElementById('greeting-div'));